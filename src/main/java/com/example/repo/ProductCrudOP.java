package com.example.repo;

import com.example.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface ProductCrudOP extends CrudRepository<Product, Integer> {

    Optional<Product> findByName(String name);

    Optional<Product> findByNameAndProductId(String name, int id);
}
