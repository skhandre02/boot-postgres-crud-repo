package com.example.bootcrudrepo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("com.example")
@EnableJpaRepositories("com.example.repo")
@EntityScan("com.example.entity")
public class BootCrudRepoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootCrudRepoApplication.class, args);
	}

}
