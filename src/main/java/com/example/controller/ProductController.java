package com.example.controller;

import com.example.entity.Product;
import com.example.repo.ProductCrudOP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;

@RestController
        @RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductCrudOP productCrudOP;

    @GetMapping
    public ResponseEntity<List<Product>> getProducts() {
        return new ResponseEntity<List<Product>>((List<Product>)productCrudOP.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable("id") int id1) {
        return new ResponseEntity<Product>(productCrudOP.findById(id1).get(), HttpStatus.OK);
    }
    @GetMapping("/name/{name}")
    public ResponseEntity<Product> getProductByName(@PathVariable String name) {
        return new ResponseEntity<Product>(productCrudOP.findByName(name).get(), HttpStatus.OK);
    }

    @GetMapping("/{id}/name/{name}")
    public ResponseEntity<Product> getProductByNameAndProductId(@PathVariable String name, @PathVariable int id) {
        Optional<Product> op = productCrudOP.findByNameAndProductId(name,id);
        if (!op.isPresent()) {
            throw new RuntimeException("NOT FOUND");
        }
        return new ResponseEntity<Product>(productCrudOP.findByNameAndProductId(name,id).get(), HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Product>  saveProduct(@RequestBody Product product) {
        return new ResponseEntity<Product>(productCrudOP.save(product), HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public void deleteProductById(@PathVariable("id") int id) {
        productCrudOP.deleteById(id);
    }
}
