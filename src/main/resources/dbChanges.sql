create database crud_repo;
create table product(product_id int primary key, product_name varchar(30));
insert into product values  (1, 'Lenovo');
insert into product values (2,'Dell');
insert into product values (3,'HP');